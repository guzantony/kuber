provider "aws" {
  region     = "eu-central-1"
}

resource "aws_vpc" "test-vpc" {
     cidr_block = "10.0.0.0/16"
      tags = {
        Name = "Test-net"
  }
}
resource "aws_subnet" "front-end-net" {
  vpc_id     = aws_vpc.test-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public-net"
  }
}
resource "aws_internet_gateway" "Test-GW" {
  vpc_id = aws_vpc.test-vpc.id

  tags = {
    Name = "Test-GW"
  }
}
resource "aws_route_table" "Test-RT" {
  vpc_id = aws_vpc.test-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.Test-GW.id
  } 

  tags = {
    Name = "Actpro-RT-Front"
  }
}
resource "aws_route_table_association" "a-front-net" {
  subnet_id      = aws_subnet.front-end-net.id
  route_table_id = aws_route_table.Test-RT.id
}
resource "aws_security_group" "master-sg" {
  name        = "master"
  description = "Allow Kube"
  vpc_id      = aws_vpc.test-vpc.id
  
  dynamic "ingress" {
    for_each = ["22", "6443", "2379", "2380", "10250", "10259", "10257"]
    content {
      from_port        = ingress.value
      to_port          = ingress.value
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]  
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "master-sg"
  }
}
resource "aws_security_group" "worker-sg" {

  vpc_id      = aws_vpc.test-vpc.id
  ingress {
    from_port        = 30000
    to_port          = 32767
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  dynamic "ingress" {
    for_each = ["22", "6443", "10250"]
    content {
        from_port        = ingress.value
        to_port          = ingress.value
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "worker-sg"
  }
}

resource "aws_security_group" "ansible-sg" {
  
  vpc_id      = aws_vpc.test-vpc.id

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}
data "aws_ami" "ubuntu-latest" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical 
}
resource "aws_instance" "ansible-server" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.ansible-sg.id]
  associate_public_ip_address = true

  key_name = "aws1"
  
  tags = {
    Name = "ansible-server"
  }
}
resource "aws_instance" "Master-server" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t3.small"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.master-sg.id]
  associate_public_ip_address = true

  key_name = "aws1"
  
  tags = {
    Name = "Master-server"
  }
}
resource "aws_instance" "Worker-node-1" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.worker-sg.id]
  associate_public_ip_address = true

  key_name = "aws1"
  
  tags = {
    Name = "worker-node-1"
  }
}
resource "aws_instance" "Worker-node-2" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.worker-sg.id]
  associate_public_ip_address = true

  key_name = "aws1"
  
  tags = {
    Name = "worker-node-2"
  }
}
resource "aws_instance" "Worker-node-3" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.worker-sg.id]
  associate_public_ip_address = true

  key_name = "aws1"
  
  tags = {
    Name = "worker-node-3"
  }
}